#ifndef __CSDN_PLATFORM_H__
#define __CSDN_PLATFORM_H__

extern int csdn_register_client(struct notifier_block *nb);
extern int csdn_unregister_client(struct notifier_block *nb);
extern int csdn_notifier_call_chain(unsigned long val, void *v);

#endif