#include <linux/init.h>
#include <linux/module.h>
#include <linux/notifier.h>

MODULE_LICENSE("GPL");

static BLOCKING_NOTIFIER_HEAD(csdn_notifier_list); /* 通知链头 */

int csdn_register_client(struct notifier_block *nb)
{
	/* 注册通知链 */
    return blocking_notifier_chain_register(&csdn_notifier_list, nb);
}
EXPORT_SYMBOL(csdn_register_client);

int csdn_unregister_client(struct notifier_block *nb)
{
	/* 卸载通知链 */
    return blocking_notifier_chain_unregister(&csdn_notifier_list, nb);
}
EXPORT_SYMBOL(csdn_unregister_client);

int csdn_notifier_call_chain(unsigned long val, void *v)
{
	/* 发送通知 */
    return blocking_notifier_call_chain(&csdn_notifier_list, val, v);
}
EXPORT_SYMBOL(csdn_notifier_call_chain);

static __init int csdn_init(void)
{
    pr_info("csdn_platform_init\n");
    return 0;
}

static __exit void csdn_exit(void)
{
    pr_info("csdn_platform_exit\n");
}

module_init(csdn_init);
module_exit(csdn_exit);