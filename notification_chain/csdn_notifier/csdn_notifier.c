#include <linux/init.h>
#include <linux/module.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/types.h>
#include <linux/kdev_t.h>
#include <linux/device.h>
#include <linux/ioctl.h>
#include <linux/uaccess.h>
#include <asm/uaccess.h>
#include "csdn_platform.h"

MODULE_LICENSE("GPL");

#define CSDN_IOC_MAGIC 'x'
#define CSDN_IOC_NOTIFIER _IOW(CSDN_IOC_MAGIC, 0, int)

static int minor = 0; /* 次设备号 */
static dev_t csdn_dev;
static struct cdev csdn_cdev;
static struct class *csdn_class = NULL;
static struct device *csdn_device = NULL;

static long csdn_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
    int retval = 0;
    int ioctl_w_value = 0;

    pr_info("csdn_notifier_ioctl");
    if (_IOC_TYPE(cmd) != CSDN_IOC_MAGIC) {
        return -ENODEV;
    }

    if (_IOC_DIR(cmd) & _IOC_READ)
		retval = !access_ok(VERIFY_WRITE, (void __user *)arg, _IOC_SIZE(cmd));
	else if (_IOC_DIR(cmd) & _IOC_WRITE)
		retval = !access_ok(VERIFY_READ, (void __user *)arg, _IOC_SIZE(cmd));

    if (retval)
		return -EFAULT;

    switch (cmd)
    {
    case CSDN_IOC_NOTIFIER:
        pr_info("CSDN_IOC_NOTIFIER \n");
        retval = copy_from_user(&ioctl_w_value, (int *)arg, sizeof(int));
        pr_info("retval: %d, ioctl_w_value: %d\n", retval, ioctl_w_value);

        csdn_notifier_call_chain(ioctl_w_value, NULL);
        break;

    default:
        pr_warn("unsupport cmd:0x%x\n", cmd);
        break;
    }

    return 0;
}

static struct file_operations csdn_fops = {
    .owner = THIS_MODULE,
    .unlocked_ioctl = csdn_ioctl,
};

/* 创建设备号 */
static int csdn_register_chrdev(void)
{
    int result;

    result = alloc_chrdev_region(&csdn_dev, minor, 1, "csdn_notifier");
    if (result < 0) {
        pr_err("alloc_chrdev_region failed! result: %d\n", result);
        return result;
    }

    return 0;
}

/* 注册驱动 */
static int csdn_cdev_add(void)
{
    int result;

    cdev_init(&csdn_cdev, &csdn_fops);
    csdn_cdev.owner = THIS_MODULE;

    result = cdev_add(&csdn_cdev, csdn_dev, 1);
    if (result < 0) {
        pr_err("alloc_chrdev_region failed! result: %d\n", result);
        unregister_chrdev_region(csdn_dev, 1);
        return result;
    }

    return 0;
}

/* 创建设备节点 */
static int csdn_device_create(void)
{
    csdn_class = class_create(THIS_MODULE, "csdn_notifier_class");
    if (IS_ERR(csdn_class)) {
        pr_err("class_create failed!\n");
        goto class_create_fail;
    }

    csdn_device = device_create(csdn_class, NULL, csdn_dev, NULL, "csdn_notifier");
    if (IS_ERR(csdn_device)) {
        pr_err("device_create failed!\n");
        goto device_create_fail;
    }

    return 0;

device_create_fail:
    class_destroy(csdn_class);
class_create_fail:
    cdev_del(&csdn_cdev);
    unregister_chrdev_region(csdn_dev, 1);
    return -1;
}

static __init int csdn_init(void)
{
    int result;

    pr_info("csdn_notifier_init\n");

    result = csdn_register_chrdev();
    if (result < 0) {
        return result;
    }

    result = csdn_cdev_add();
    if (result < 0) {
        return result;
    }

    result = csdn_device_create();
    if (result < 0) {
        return result;
    }

    return 0;
}

static __exit void csdn_exit(void)
{
    pr_info("csdn_notifier_exit\n");

    device_destroy(csdn_class, csdn_dev);
    class_destroy(csdn_class);
    cdev_del(&csdn_cdev);
    unregister_chrdev_region(csdn_dev, 1);
}

module_init(csdn_init);
module_exit(csdn_exit);