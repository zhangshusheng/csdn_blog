#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>

#define DEVICE_NAME "/dev/csdn_notifier"

#define CSDN_IOC_MAGIC 'x'
#define CSDN_IOC_NOTIFIER _IOW(CSDN_IOC_MAGIC, 0, int)

int main(int argc, char const *argv[])
{
    int key = 0;

    int fd = open(DEVICE_NAME, O_RDWR);
    if (fd < 0) {
        printf("open failed! error:%s\n", strerror(errno));
        return -1;
    }

    while (1) {
        printf("input> ");
        scanf("%d", &key);
        getchar();

        if (ioctl(fd, CSDN_IOC_NOTIFIER, &key) < 0) {
            printf("ioctl CSDN_IOC_NOTIFIER failed!\n");
            return -1;
        }
    }

    close(fd);
    return 0;
}
