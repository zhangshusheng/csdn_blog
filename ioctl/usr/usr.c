#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>

#define DEVICE_NAME "/dev/csdn_dev"

#define CSDN_IOC_MAGIC 'x'
#define CSDN_IOC_GET _IO(CSDN_IOC_MAGIC, 0)
#define CSDN_IOC_W _IOW(CSDN_IOC_MAGIC, 1, int)
#define CSDN_IOC_R _IOR(CSDN_IOC_MAGIC, 2, int)
#define CSDN_IOC_WR _IOWR(CSDN_IOC_MAGIC, 3, int)

int main(int argc, char const *argv[])
{
    int fd = open(DEVICE_NAME, O_RDWR);
    if (fd < 0) {
        printf("open failed! error:%s\n", strerror(errno));
        return 0;
    }

    if (ioctl(fd, CSDN_IOC_GET) < 0) {
        printf("ioctl CSDN_IOC_GET failed!\n");
        return 0;
    }

    int result = 1001;
    if (ioctl(fd, CSDN_IOC_W, &result) < 0) {
        printf("ioctl CSDN_IOC_W failed!\n");
        return 0;
    }

    if (ioctl(fd, CSDN_IOC_R, &result) < 0) {
        printf("ioctl CSDN_IOC_R failed!\n");
        return 0;
    }
    printf("CSDN_IOC_R result: %d\n", result);

    result = 2002;
    if (ioctl(fd, CSDN_IOC_WR, &result) < 0) {
        printf("ioctl CSDN_IOC_R failed!\n");
        return 0;
    }
    printf("CSDN_IOC_WR result: %d\n", result);

    close(fd);

    return 0;
}
