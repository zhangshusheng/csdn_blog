#include <linux/init.h>
#include <linux/module.h>
#include <linux/platform_device.h>

MODULE_LICENSE("GPL");

static struct resource csdn_resource[] = {
    [0] = {
        .start = 0x114000a0,
        .end = 0x114000a0 + 0x4,
        .flags = IORESOURCE_MEM,
    },
};

static void csdn_device_release(struct device *dev)
{
	printk("csdn_device_release\n");
}

static struct platform_device csdn_device = {
    .name = "csdn_blog",
    .id = -1,
    .dev.release = csdn_device_release,
    .num_resources = ARRAY_SIZE(csdn_resource),
    .resource = csdn_resource,
};

static __init int csdn_device_init(void)
{
    pr_info("csdn_device_init\n");
    return platform_device_register(&csdn_device);
}

static __exit void csdn_device_exit(void)
{
    pr_info("csdn_device_exit\n");
    platform_device_unregister(&csdn_device);
}

module_init(csdn_device_init);
module_exit(csdn_device_exit);