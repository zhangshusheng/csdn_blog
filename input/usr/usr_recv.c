#include <linux/input.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#define  CSDN_INPUT_DEV "/dev/input/event16"

int main(int argc, char const *argv[])
{
    int ret;
    struct input_event ev;

    int fd = open(CSDN_INPUT_DEV, O_RDONLY);
    if (fd < 0) {
        printf("open failed! error:%s\n", strerror(errno));
        return 0;
    }

    while (1) {
        memset(&ev, 0, sizeof(struct input_event));
        ret = read(fd, &ev, sizeof(struct input_event));
        if  (ret != sizeof(struct input_event)) {
            printf("read error, ret: %d\n", ret);
            break;
        }

        printf("--------------------\n");
        printf("type = %u.\n", ev.type);
        printf("code = %u.\n", ev.code);
        printf("value = %u.\n", ev.value);

        if (ev.type == EV_KEY) {
            if (ev.value == 1) {
                printf("down!!!\n");
            } else {
                printf("up!!!\n");
            }
        }
    }

    close(fd);

    return 0;
}
