#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>

#define DEVICE_NAME "/dev/csdn_dev"

#define CSDN_IOC_MAGIC 'x'
#define CSDN_IOC_GET _IO(CSDN_IOC_MAGIC, 0)
#define CSDN_IOC_W _IOW(CSDN_IOC_MAGIC, 1, int)
#define CSDN_IOC_R _IOR(CSDN_IOC_MAGIC, 2, int)
#define CSDN_IOC_WR _IOWR(CSDN_IOC_MAGIC, 3, int)
#define CSDN_IOC_INPUT _IOW(CSDN_IOC_MAGIC, 4, int)

int main(int argc, char const *argv[])
{
    int cmd = 0;
    int key = 0;

    int fd = open(DEVICE_NAME, O_RDWR);
    if (fd < 0) {
        printf("open failed! error:%s\n", strerror(errno));
        return 0;
    }

    while (1) {
        printf("input> ");
        scanf("%d", &cmd);
        getchar();

        switch (cmd) {
            case 0:
                key = 0;
            break;

            case 1:
                key = 1;
            break;

            default:
                key = 0;
            break;
        }

        if (ioctl(fd, CSDN_IOC_INPUT, &key) < 0) {
            printf("ioctl CSDN_IOC_INPUT failed!\n");
            return 0;
        }
    }

    close(fd);

    return 0;
}
